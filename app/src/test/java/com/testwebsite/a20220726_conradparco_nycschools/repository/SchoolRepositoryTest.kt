package com.testwebsite.a20220726_conradparco_nycschools.repository

import com.testwebsite.a20220726_conradparco_nycschools.api.SchoolApi
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import junit.framework.TestCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

internal class SchoolRepositoryTest {

    @MockK
    private lateinit var api: SchoolApi
    @MockK
    private lateinit var school: School
    private val dispatcher = Dispatchers.Unconfined
    private lateinit var repo: SchoolRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repo = SchoolRepository(api)
    }

    @Test
    fun `fetch schools and test that data is cached`() = runBlocking {
        // Given
        val schools = listOf(school)
        coEvery { api.fetchSchools() } returns schools

        // When
        repo.fetchSchools()

        // Then
        coVerify { api.fetchSchools() }
        assertEquals(1, repo.schools.size)
    }
}