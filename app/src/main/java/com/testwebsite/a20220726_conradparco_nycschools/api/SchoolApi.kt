package com.testwebsite.a20220726_conradparco_nycschools.api

import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.data.Score
import retrofit2.http.GET

interface SchoolApi {
    @GET("s3k6-pzi2.json")
    suspend fun fetchSchools() : List<School>

    @GET("f9bf-2cp4.json")
    suspend fun fetchScores(): List<Score>
}