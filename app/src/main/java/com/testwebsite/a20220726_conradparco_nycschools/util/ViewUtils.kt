package com.testwebsite.a20220726_conradparco_nycschools.util

import android.view.View
import android.widget.ProgressBar

fun ProgressBar.show() {
    this.visibility = View.VISIBLE
}

fun ProgressBar.gone() {
    this.visibility = View.GONE
}