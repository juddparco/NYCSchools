package com.testwebsite.a20220726_conradparco_nycschools.di

import com.testwebsite.a20220726_conradparco_nycschools.api.SchoolApi
import com.testwebsite.a20220726_conradparco_nycschools.repository.SchoolRepository
import com.testwebsite.a20220726_conradparco_nycschools.repository.SchoolRepositoryContract
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//TODO Use Dagger to replace this
object NetworkModule {

    // TODO Replace with Build injected url
    private val apiUrl = "https://data.cityofnewyork.us/resource/"

    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .build()
    }

    private val retrofit by lazy {
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(apiUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val schoolApi: SchoolApi by lazy {
        retrofit.create(SchoolApi::class.java)
    }

    val schoolRepo: SchoolRepositoryContract by lazy {
        SchoolRepository(schoolApi)
    }
}