package com.testwebsite.a20220726_conradparco_nycschools.data

sealed class NetworkResponse<T> {
    data class Success<T>(val data: T) : NetworkResponse<T>()
    data class Error<T>(val error: Throwable? = null, val data: T? = null): NetworkResponse<T>()
    class Loading<T> : NetworkResponse<T>()
}
