package com.testwebsite.a20220726_conradparco_nycschools.repository

import com.testwebsite.a20220726_conradparco_nycschools.api.SchoolApi
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.data.Score
import com.testwebsite.a20220726_conradparco_nycschools.di.NetworkModule
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface SchoolRepositoryContract {

    var selectedSchool: School?

    suspend fun fetchSchools(): List<School>

    suspend fun fetchScores(schoolId: String): Score?
}

class SchoolRepository(
    private val api: SchoolApi = NetworkModule.schoolApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : SchoolRepositoryContract {

    // TODO these are temporary caches that are in-mem. Add real caching later
    internal val schools = mutableListOf<School>()
    private val scores = mutableListOf<Score>()
    @Volatile
    override var selectedSchool: School? = null

    override suspend fun fetchSchools(): List<School> = withContext(dispatcher){
        if (schools.isNotEmpty()) return@withContext schools
        return@withContext api.fetchSchools().also {
            this@SchoolRepository.schools.addAll(it)
        }
    }

    // This method searches the cache first for the score
    override suspend fun fetchScores(schoolId: String): Score? = withContext(dispatcher) {
        val localScore = fetchScoreFromCache(schoolId)
        if (localScore != null) return@withContext localScore
        val remoteScores =  api.fetchScores()
        synchronized(this) {
            this@SchoolRepository.scores.apply {
                clear()
                addAll(remoteScores)
            }
        }
        return@withContext fetchScoreFromCache(schoolId)
    }

    private suspend fun fetchScoreFromCache(dbn: String) = scores.firstOrNull { it.dbn == dbn}

}