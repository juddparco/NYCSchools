package com.testwebsite.a20220726_conradparco_nycschools.ui.schools

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testwebsite.a20220726_conradparco_nycschools.data.NetworkResponse
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.repository.SchoolRepositoryContract
import kotlinx.coroutines.async

class SchoolsViewModel(
    private val repo: SchoolRepositoryContract
) : ViewModel() {

    private val _schools = MutableLiveData<NetworkResponse<List<School>>>()
    val schools: LiveData<NetworkResponse<List<School>>> get() = _schools

    fun fetchSchools() {
        _schools.postValue(NetworkResponse.Loading())
        viewModelScope.async {
            try {
                _schools.postValue(NetworkResponse.Success(repo.fetchSchools()))
            } catch (e: Throwable) {
                _schools.postValue(NetworkResponse.Error(e))
            }

        }
    }

    fun schoolSelected(school: School) {
        repo.selectedSchool = school
    }
}