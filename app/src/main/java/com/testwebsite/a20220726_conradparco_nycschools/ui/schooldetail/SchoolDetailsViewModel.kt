package com.testwebsite.a20220726_conradparco_nycschools.ui.schooldetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testwebsite.a20220726_conradparco_nycschools.data.NetworkResponse
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.data.Score
import com.testwebsite.a20220726_conradparco_nycschools.repository.SchoolRepositoryContract
import kotlinx.coroutines.async
import java.lang.IllegalArgumentException

class SchoolDetailsViewModel(
    private val repo: SchoolRepositoryContract
) : ViewModel() {

    private val _metadata = MutableLiveData<NetworkResponse<SchoolMetaData>>()
    val metadata: LiveData<NetworkResponse<SchoolMetaData>> get() = _metadata

    fun fetchSchoolAndScores() {
        _metadata.postValue(NetworkResponse.Loading())
        viewModelScope.async {
            repo.selectedSchool?.let {
                fetchScore(it)
            }
                ?: _metadata.postValue(NetworkResponse.Error(IllegalArgumentException("No school found")))
        }
    }

    private suspend fun fetchScore(school: School) {
        try {
            val score = repo.fetchScores(school.dbn ?: "")
            when {
                score == null -> _metadata.postValue(
                    NetworkResponse.Error(IllegalArgumentException("No scores found"), SchoolMetaData(school, null))
                )
                else -> _metadata.postValue(NetworkResponse.Success(SchoolMetaData(school, score)))
            }
        } catch (e: Throwable) {
            _metadata.postValue(NetworkResponse.Error(e))
        }
    }
}

data class SchoolMetaData(
    val school: School,
    val scores: Score?
)