package com.testwebsite.a20220726_conradparco_nycschools.ui.schools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.testwebsite.a20220726_conradparco_nycschools.di.NetworkModule
import java.lang.IllegalArgumentException

class SchoolViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolsViewModel::class.java)) {
            return SchoolsViewModel(NetworkModule.schoolRepo) as T
        }
        throw IllegalArgumentException("Need a viewmodule assignable")
    }
}