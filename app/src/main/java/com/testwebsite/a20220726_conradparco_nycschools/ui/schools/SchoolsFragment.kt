package com.testwebsite.a20220726_conradparco_nycschools.ui.schools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.testwebsite.a20220726_conradparco_nycschools.R
import com.testwebsite.a20220726_conradparco_nycschools.data.NetworkResponse
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.databinding.FragmentSchoolsBinding
import com.testwebsite.a20220726_conradparco_nycschools.ui.schools.adapter.SchoolAdapter
import com.testwebsite.a20220726_conradparco_nycschools.util.gone
import com.testwebsite.a20220726_conradparco_nycschools.util.show

class SchoolsFragment : Fragment() {

    private var _binding: FragmentSchoolsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolsViewModel by lazy { ViewModelProvider(this,SchoolViewModelFactory())[SchoolsViewModel::class.java]}
    private val schoolAdapter by lazy {
        SchoolAdapter {
            viewModel.schoolSelected(it)
            findNavController().navigate(R.id.schools_to_details_action)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSchoolsBinding.inflate(inflater, container, false).apply {
        _binding = this
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
        observe()
        viewModel.fetchSchools()
    }

    private fun setViews() {
        binding.recyclerView.apply {
            adapter = schoolAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observe() {
        viewModel.schools.observe(viewLifecycleOwner) {
            when(it) {
                is NetworkResponse.Success -> handleSuccess(it)
                is NetworkResponse.Error -> handleError(it)
                is NetworkResponse.Loading -> showLoading()
            }
        }
    }

    private fun handleSuccess(success: NetworkResponse.Success<List<School>>) {
        stopShowingLoading()
        schoolAdapter.update(success.data)
    }


    // TODO Log errors properly later
    private fun handleError(error: NetworkResponse.Error<List<School>>) {
        stopShowingLoading()
        Snackbar.make(binding.root, R.string.school_loading_error, Snackbar.LENGTH_LONG).apply {
            setAction(R.string.retry) {
                viewModel.fetchSchools()
            }
            show()
        }
    }

    private fun showLoading() {
        binding.progressBar.show()
    }

    private fun stopShowingLoading() {
        binding.progressBar.gone()
    }
}