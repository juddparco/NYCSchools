package com.testwebsite.a20220726_conradparco_nycschools.ui.schools.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testwebsite.a20220726_conradparco_nycschools.data.School

class SchoolAdapter(
    private val listener: (School) -> Unit
) : RecyclerView.Adapter<SchoolViewHolder>() {

    private val items = mutableListOf<School>()

    // TODO Use DiffUtils later to be more efficient
    fun update(schools: List<School>) {
        items.clear()
        items.addAll(schools)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SchoolViewHolder(parent) { position -> listener(items[position])}

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bindModelToView(items[position])
    }

    override fun getItemCount() = items.size
}