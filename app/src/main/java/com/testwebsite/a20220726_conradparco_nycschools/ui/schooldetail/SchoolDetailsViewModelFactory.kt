package com.testwebsite.a20220726_conradparco_nycschools.ui.schooldetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.testwebsite.a20220726_conradparco_nycschools.di.NetworkModule
import java.lang.IllegalArgumentException

class SchoolDetailsViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolDetailsViewModel::class.java)) {
            return SchoolDetailsViewModel(NetworkModule.schoolRepo) as T
        }
        throw IllegalArgumentException("Need a viewmodule assignable")
    }
}