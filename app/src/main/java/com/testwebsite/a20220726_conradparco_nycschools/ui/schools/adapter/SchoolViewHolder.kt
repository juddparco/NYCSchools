package com.testwebsite.a20220726_conradparco_nycschools.ui.schools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.databinding.RowSchoolItemBinding


class SchoolViewHolder(
    private val parent: ViewGroup,
    private val binding: RowSchoolItemBinding = RowSchoolItemBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    private val listener: (Int) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bindModelToView(model: School) {
        binding.title.text = model.schoolName
        binding.description.text = model.overviewParagraph
        binding.detailsButton.setOnClickListener {
            listener(adapterPosition)
        }
    }
}