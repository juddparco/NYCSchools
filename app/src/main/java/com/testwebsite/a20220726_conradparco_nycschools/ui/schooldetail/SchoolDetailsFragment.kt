package com.testwebsite.a20220726_conradparco_nycschools.ui.schooldetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.testwebsite.a20220726_conradparco_nycschools.R
import com.testwebsite.a20220726_conradparco_nycschools.data.NetworkResponse
import com.testwebsite.a20220726_conradparco_nycschools.data.School
import com.testwebsite.a20220726_conradparco_nycschools.data.Score
import com.testwebsite.a20220726_conradparco_nycschools.databinding.FragmentSchoolDetailsBinding
import com.testwebsite.a20220726_conradparco_nycschools.databinding.FragmentSchoolsBinding
import com.testwebsite.a20220726_conradparco_nycschools.ui.schools.SchoolViewModelFactory
import com.testwebsite.a20220726_conradparco_nycschools.ui.schools.SchoolsViewModel
import com.testwebsite.a20220726_conradparco_nycschools.util.gone
import com.testwebsite.a20220726_conradparco_nycschools.util.show

class SchoolDetailsFragment : Fragment() {

    private var _binding: FragmentSchoolDetailsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolDetailsViewModel by lazy {
        ViewModelProvider(
            this,
            SchoolDetailsViewModelFactory()
        )[SchoolDetailsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSchoolDetailsBinding.inflate(inflater, container, false).apply {
        _binding = this
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe()
        viewModel.fetchSchoolAndScores()
    }

    private fun observe() {
        viewModel.metadata.observe(viewLifecycleOwner) {
            when (it) {
                is NetworkResponse.Success -> onSuccess(it)
                is NetworkResponse.Error -> handleError(it)
                is NetworkResponse.Loading -> binding.progressBar.show()
            }
        }
    }

    private fun onSuccess(success: NetworkResponse.Success<SchoolMetaData>) {
        binding.progressBar.gone()
        setSchoolDetails(success.data.school)
        setScoreDetails(success.data.scores)
    }

    private fun setScoreDetails(scores: Score?) {
        binding.mathScore.text = scores?.satMathAvgScore.getScore()
        binding.writingScore.text = scores?.satWritingAvgScore.getScore()
        binding.readingScore.text = scores?.satCriticalReadingAvgScore.getScore()
    }

    private fun setSchoolDetails(school: School) {
        binding.title.text = school.schoolName
        binding.description.text = school.overviewParagraph
    }

    private fun String?.getScore(): String = when {
        this == null -> getString(R.string.not_available)
        this.equals("s", true) -> getString(R.string.not_available)
        else -> this
    }

    // TODO Log errors properly later
    private fun handleError(error: NetworkResponse.Error<SchoolMetaData>) {
        binding.progressBar.gone()
        Snackbar.make(binding.root, R.string.school_loading_error, Snackbar.LENGTH_LONG).apply {
            setAction(R.string.retry) {
                viewModel.fetchSchoolAndScores()
            }
            show()
        }
        if (error.data != null) {
            setSchoolDetails(error.data.school)
            setScoreDetails(error.data.scores)
        }
    }
}